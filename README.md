# Hª_de_los_algoritmos_y_de_los_lenguajes_de_programación__2010
```plantuml
@startmindmap
* Hª de los algoritmos y de los lenguajes de programación (2010)
** El primer lenguaje de programación nació en 1801
***_ creado por
**** Joseph Marie Jacquard
***** inventor del telar programable.
*** tarjetas programables 
****  introducídas en un telar que leía el código
*** primeras máquinas computacionales
****_ creado por
***** Charles Babbage
** El primer lenguaje de programación, el de Ada Lovelace
***_ propuso 
**** creó un lenguaje de programación a mano para calcular la serie de números de Bernoulli con la máquina de Babbage.
**  1936, Alan Turing
***_ creo
**** Máquina de Turing
*** máquina que demostraba que, con un algoritmo, podían resolver cualquier problema matemático. 
** 1957 y el primer lenguaje de programación real
*** el primer lenguaje de programación de alto nivel
***_ creado por 
**** John W.Backus inventa
** En 1959 llegó COBOL
*** a culminación de varios otros sistemas de programación
*** permitió modernizar la informática de gestión.
** 1964 y el nacimiento de BASIC
*** Beginner's All-purpose Symbolic Instruction Code
** 1972, llega C
***_ creado por 
**** Dennis Ritchie
*** lenguaje de programación en el que solo unas pocas instrucciones pueden traducir cada elemento del lenguaje. 
** Y C++ en 1979
*** Bjarne Stroustrup
*** extender al lenguaje de programación C mecanismos para manipular objetos. 
** Scratch en 2006
** 2009, Go, de Google
*** también es conocido como Goland
*** lenguaje de programación diseñado por Google
** Kotlin, en 2012
*** es uno de los lenguajes de programación de moda
*** como el mejor lenguaje de programación para Android.
** Swift, en 2013
***  han creado diferentes lenguajes específicos para agilizar la programación.
*** reado por Apple, es un lenguaje de programación clave para programar en iOS.
@endmindmap
```

# La evolución de los lenguajes y paradigmas de programación
```plantuml
@startmindmap
* La evolución de los lenguajes \ny paradigmas de programación (2005)
** lenguaje \nde programación
***_ es
**** definido como un idioma artificial,\n formado por símbolos\ny reglas sintácticas \n y semánticas que \n pueden usarse para crear programas.

** Fortran (1957)
*** el lenguaje de programación más antiguo 
***_ Creada por 
**** John Backus
*** desarrollada para la computación científica de alto nivel
** Cobol (1959)
***_ creado por
**** Grace Murray Hopper
*** Common Business Oriented Language
*** sistemas de transacciones de negocio
** Basic (1964)
***_ creado por
**** estudiantes de Dartmouth College
*** lenguaje simplificado para aquellos que \n no tenían como base fuertes conocimientos\n técnicos o matemáticos
*** integrado en el Apple II para su arranque
** C (1969)
*** fue desarrollado entre 1969 y 1973
***_ creado por 
**** Dennis Ritchie
*** para los laboratorios Bell Telephone 
***  sistema Unix
***  Linux está todavía basado en C.
** Pascal (1970) 
*** se llamó así en honor de Blaise Pascal
*** inventor de las calculadoras matemáticas
***_ creador
**** Niklaus Wirth
*** Este lenguaje se usa en Skype.
** Perl (1987)
***_ creado por
**** Larry Wall
*** Practical Extraction Report Language
**** consigue que hagas tu trabajo
*** Actualmente es usado por Craigslist.
** Python (1991)
*** Monty Python sirvió de inspiración para nombrar este lenguaje
*** Guido Van Rossum
*** creado para solucionar problemas en el lenguaje ABC 
*** Hoy en día es usado por la NASA, Google y YouTube
** Ruby (1993)
***  creado por Yukihiro Matsumoto
***  utilizando partes de sus lenguajes de programación favoritos: \n Perl, Smalltalk, \n Eiffel, Ada y Lisp
*** Ahora es usado por Basecamp.
** PHP (1995)
*** desarrollado por Rasmus Lerdoff
*** reemplazar unos scripts de Perl\n usados para mantener su web personal 
*** Hoy en día, PHP ha crecido hasta llegar\n a ser parte de una arquitectura \n web integrada en 20 millones de websites
***  Facebook lo usa actualmente.
** Javascript (1995)
*** desarrollada por Brendan Eich
*** iene influencia del lenguaje C
*** Hoy en día es usado en servicios como node.js. De él depende AJAX.
** Ruby On Rails (2005)
*** Fue extraído por David Heinemeier Hansson
*** Hasson lanzó Ruby On Rails
*** código abierto en  el 2004
*** no compartió los derechos hasta febrero de 2005
***  Ahora está en su versión 3.0.7 y tiene más de 1.800 contribuyentes.
@endmindmap

```
# Programando ordenadores en los 80 y ahora.
```plantuml
@startmindmap
* Programando ordenadores en los 80 y ahora.
** Almacenamiento externo
*** El disquete era el líder
*** disquete de 3-1/2
*** CD-ROM
*** SuperDisk
*** disquetes de baja densidad
**** (720KB) 
*** disquetes de alta densidad
**** (1.44MB)
** Antivirus
*** objetivo de los virus era destruir información
**** DrWeb
**** McAfee ViruScan
***** administradores de sistemas
** Manipulación de archivos
*** uso de disquetes
**** ARJ
**** LZH
*** CD-ROM
*** productos de Microsoft
**** EXPAND.EXE
** Multimedia (Sonido)
***_ sistemas en ese tiempo
**** PC Speaker
**** trackers musicales
*****_ crear
****** composiciones musicales
**** Scream Tracker
**** Impulse Tracker
**** Fast Tracker 
** Mantenimiento
*** Scandisk
****_ era
***** un programa «gráfico» de Microsoft que mostraba un mapa del disco a examinar
*** Defrag 
****_ era
***** un software que comprimía y descomprimía los archivos sobre la marcha
** Memoria RAM
***_ se utilizaba
**** memoria extendida
**** DriveSpace 
****  MemMaker 
*****_ se usaba
****** optimizar el uso de Memoria RAM 
** Multimedia
*** formato FLIC
*** Autodesk Animator
*** Deluxe Paint II
*** capturas de pantalla 
**** Winamp
**** Paint
**** Word
**** WinZip
** Utilidades de disco
*** DOS Shell W
*** PC Shell
*** PC Tools
*** DOSShell
** Entretenimiento
*** limitado gráficamente
***_ durante los 90
**** miles de juegos convirtieron los PCs domésticos en el dispositivo más completo para trabajar y, también, para divertirse.
@endmindmap
```
